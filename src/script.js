import './style.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import * as dat from 'dat.gui'
import { Scene, TextureLoader } from 'three'
import { Text } from 'troika-three-text'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'
//import typefaceFont from 'three/examples/fonts/helvetiker_regular.typeface.json'
const loadingManager = new THREE.LoadingManager()
const textureLoader = new THREE.TextureLoader(loadingManager)

import imageContacto from '../static/contacto.jpg'
import imageFilosofia from '../static/filosofia.jpg';
import imagePortfolio from '../static/portfolio.jpg';
const textureContacto = textureLoader.load(imageContacto)
const textureFilosofia = textureLoader.load(imageFilosofia);
const texturePortfolio = textureLoader.load(imagePortfolio);

const gui = new dat.GUI()
    /**
     * Base
     */
    // Canvas
const canvas = document.querySelector('canvas.webgl')


/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

// Scene
let scene = new THREE.Scene()
let sceneFilosofia = new THREE.Scene();
let sceneContacto = new THREE.Scene();
let scenePortfolio = new THREE.Scene();
/*const cube2 = new THREE.Mesh(
    new THREE.BoxGeometry(1, 1, 1),
    new THREE.MeshBasicMaterial({ color: 0x00ff00 }),
);
*/
const directionalLight = new THREE.DirectionalLight(0xFFFFFF, 1);
directionalLight.position.set(0, 0, 0);
directionalLight.target.position.set(0, 0, 0);

const light = new THREE.PointLight(0xFFFFFF, 1);
light.position.set(0, 0, 0);
sceneContacto.add(light);

sceneContacto.add(directionalLight);
sceneContacto.add(directionalLight.target);

const particlesGeometry = new THREE.BufferGeometry();
const count = 5000;

const positions = new Float32Array(count * 3);

for (let i = 0; i < count * 3; i++) {
    positions[i] = (Math.random() - 0.5) * 10
}

particlesGeometry.setAttribute(
    'position',
    new THREE.BufferAttribute(positions, 3)
);

const particlesMaterial = new THREE.PointsMaterial();
particlesMaterial.size = 0.02;
particlesMaterial.sizeAttenuation = true;
particlesMaterial.transparent = true;
//particlesMaterial.alphaTest = 0.001;
particlesMaterial.depthWrite = false;

const particles = new THREE.Points(particlesGeometry, particlesMaterial);
scene.add(particles);

const gltfLoader = new GLTFLoader();
gltfLoader.load(
        '/models/email/scene.gltf',
        (gltf) => {
            console.log(gltf);

            while (gltf.scene.children.length) {
                //gltf.scene.children[0].position.set(camera.position);
                //gltf.scene.rotation.set(90, 0, 0);
                //gltf.scene.children[0].scale.set(2, 2, 2);
                gltf.scene.children[0].rotation.x += 2 * Math.PI
                sceneContacto.add(gltf.scene.children[0]);
                //gltf.scene.children[0].rotation.set(0, 90, 0);
            }

            //sceneContacto.add(gltf.scene);

        }
    )
    /*
    const createText = (font, str) => {
        const textGeometryFilosofia = new THREE.TextGeometry(str, {...textOptions, font: font })

        const textMaterial = new THREE.MeshBasicMaterial({
            color: 0x0000ff
        })

        textGeometryFilosofia.center()

        textMesh = new THREE.Mesh(textGeometryFilosofia, textMaterial)
        textMesh.castShadow = true
        sceneFilosofia.add(textMesh)
    }
    */
    /*
    const loader = new THREE.FontLoader();
    loader.load('/fonts/helvetiker_regular.typeface.json', (f) => {
        font = f,
            createText(
                font,
                'La Filosofía que seguimos es siempre estar al tanto de lo último en Tecnología y aplicarla, todo esto intentando lograr un precio justo tanto como para nuestros Clientes, como para también nosotros.'
            );
    })
    */
const textoFilosofia = new Text();
sceneFilosofia.add(textoFilosofia);

textoFilosofia.text =
    'Nuestro Objetivo sería crear el Mejor Software, ésto no siempre es barato pues significa utilizar lo último en Tecnología\n y además muchos conocimientos, pero no por éste motivo somos inconscientes y le agregamos un valor imposible\n en nuestros Proyectos, simplemente lo justo.    La Filosofía que seguimos es siempre estar al tanto de lo último en\n Tecnología y aplicarla, todo esto intentando lograr un precio justo tanto como para nuestros Clientes, como para\n también nosotros. Normalmente utilizamos una o seguramente una combinación de muchas de éstas Tecnologías,\n básicamente son las siguientes y sus sub - ramas no mencionadas aqui. También brindamos servicios de Pentesting\n en Vulnerabilidades Web, de Red, Systema Operativo o ótras áreas.Adicionalmente creamos Interfaces gráficas,\n ya sea en Desarrollos Web, Aplicaciones de Escritorio y Videojuegos ó Desarrollos completos en 3 D. Nuestros\n Desarrollos Web y Aplicaciones son 100 % Responsivos, ésto es que sin importar si es una Página Web en una\n pantalla enorme, un celular de cualquier marca, una tablet, el contenido siempre se mostrará adecuadamente. Siempre\n procuramos que todo Proyecto elaborado sea Multiplataforma, es decir, que sin importar que usted utilice el Sistema\n Operativo Windows, Mac, Linux, su celular Android o su iPhone, nuestro trabajo también esté enfocado a éstas Plataformas.\n Ésto no siempre es posible y generalmente se tiene que cobrar por cada una de éstas, pues significa utilizar un nuevo lenguaje,\n tecnología o librería para cada uno. En cuanto a Asesoría también la brindamos, todas las Empresas generan un cargo por éste\n concepto, nosotros lo hacemos gratis si usted pretende contratar nuestros servicios, pero sí tiene un costo. A lo largo de los años\n hemos realizado tantos Desarrollos que creo que no recordamos todos con exactitud, pero en nuestra sección Portafolio mostramos\n algunos de ellos(únicamente los más recientes).';
textoFilosofia.fontSize = 0.2;
//textoFilosofia.position.z = -2,
textoFilosofia.color = 0xffffff;
textoFilosofia.position.set(-5, 2, 0);

/*
loader.load('/fonts/helvetiker_regular.typeface.json', (font) => {
    const textGeometryFilosofia = new THREE.TextBufferGeometry('Filosofia', {
        font: font,
        size: 0.2,
        height: 0.1,
        weight: 'normal',
        curveSegments: 12,
        bevelEnabled: true,
        bevelThickness: 0.03,
        bevelSize: 0.02,
        bevelOffset: 0,
        bevelSegments: 5

    })

    const textGeometryParagraph = new THREE.TextBufferGeometry(
        'La Filosofía que seguimos es siempre estar al tanto de lo último en Tecnología y aplicarla, todo esto intentando lograr un precio justo tanto como para nuestros Clientes, como para también nosotros.', {
            font: font,
            size: 0.1,
            height: 0.05,
            weight: 'normal',
            curveSegments: 12,
            bevelEnabled: true,
            bevelThickness: 0.03,
            bevelSize: 0.02,
            bevelOffset: 0,
            bevelSegments: 5
        })

    let textMaterial = new THREE.MeshBasicMaterial({ color: 0x11ff00 });

    let textoFilosofia = new THREE.Mesh(textGeometryFilosofia, textMaterial);
    sceneFilosofia.add(textoFilosofia);

    let textoFilosofiaParagraph = new THREE.Mesh(textGeometryParagraph, textMaterial);
    sceneFilosofia.add(textoFilosofiaParagraph);

    let aLight = new THREE.AmbientLight(0x404040, 5);
    sceneFilosofia.add(aLight);

});
*/
//scene2.add(cube2);

const raycaster = new THREE.Raycaster();



/*const rayOrigin = new THREE.Vector3(-3, 0, 0);
const rayDirection = new THREE.Vector3(10, 0, 0);
rayDirection.normalize();
raycaster.set(rayOrigin, rayDirection);
*/
window.addEventListener('dblclick', () => {
    if (!document.fullscreenElement) {
        canvas.requestFullscreen()
    } else {
        document.exitFullscreen()
    }
})

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
camera.position.x = 1
camera.position.y = 1
camera.position.z = 4
scene.add(camera)

// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true

/**
 * Cube
 */
const cubeFilosofia = new THREE.Mesh(
    new THREE.BoxGeometry(2, 2, 2),
    new THREE.MeshBasicMaterial({ map: textureFilosofia })
)

const sphereContacto = new THREE.Mesh(
    new THREE.SphereGeometry(1, 32, 32),
    new THREE.MeshBasicMaterial({ map: textureContacto })
);

const dodecahedronPortfolio = new THREE.Mesh(
    new THREE.DodecahedronGeometry(1),
    new THREE.MeshBasicMaterial({ map: texturePortfolio })
)

const texturePgbUno = textureLoader.load('/portfolio/pgbinicio.png');
const materialPortfolio = new THREE.MeshBasicMaterial();
materialPortfolio.map = texturePgbUno;
materialPortfolio.side = THREE.DoubleSide;
const planePgbUno = new THREE.Mesh(
    new THREE.PlaneBufferGeometry(1, 1, 100, 100),
    materialPortfolio,
);

const texturePgbQuienes = textureLoader.load('/portfolio/pgb-quienes.png');
const materialPortfolioQuienes = new THREE.MeshBasicMaterial();
materialPortfolioQuienes.map = texturePgbQuienes;
materialPortfolioQuienes.side = THREE.DoubleSide;
const planePgbQuienes = new THREE.Mesh(
    new THREE.PlaneBufferGeometry(1, 1, 100, 100),
    materialPortfolioQuienes,
);

const texturePgbContacto = textureLoader.load('/portfolio/pgb-contacto.png');
const materialPortfolioContacto = new THREE.MeshBasicMaterial();
materialPortfolioContacto.map = texturePgbContacto;
materialPortfolioContacto.side = THREE.DoubleSide;
const planePgbContacto = new THREE.Mesh(
    new THREE.PlaneBufferGeometry(1, 1, 100, 100),
    materialPortfolioContacto,
);

const texturePgbSeguros = textureLoader.load('/portfolio/pgb-seguros.png');
const materialPortfolioSeguros = new THREE.MeshBasicMaterial();
materialPortfolioSeguros.map = texturePgbContacto;
materialPortfolioSeguros.side = THREE.DoubleSide;
const planePgbSeguros = new THREE.Mesh(
    new THREE.PlaneBufferGeometry(1, 1, 100, 100),
    materialPortfolioSeguros,
);

const textureFfInicio = textureLoader.load('/portfolio/ffInicio.png');
const materialPortfolioFfInicio = new THREE.MeshBasicMaterial();
materialPortfolioFfInicio.map = textureFfInicio;
materialPortfolioFfInicio.side = THREE.DoubleSide;
const planeFfInicio = new THREE.Mesh(
    new THREE.PlaneBufferGeometry(1, 1, 100, 100),
    materialPortfolioFfInicio,
);

const textureFfColecciones = textureLoader.load('/portfolio/ffcolecciones.png');
const materialPortfolioFfColecciones = new THREE.MeshBasicMaterial();
materialPortfolioFfColecciones.map = textureFfColecciones;
materialPortfolioFfColecciones.side = THREE.DoubleSide;
const planeFfColecciones = new THREE.Mesh(
    new THREE.PlaneBufferGeometry(1, 1, 100, 100),
    materialPortfolioFfColecciones,
);

const textureFfFrutales = textureLoader.load('/portfolio/fffrutales.png');
const materialPortfolioFfFrutales = new THREE.MeshBasicMaterial();
materialPortfolioFfFrutales.map = textureFfFrutales;
materialPortfolioFfFrutales.side = THREE.DoubleSide;
const planeFfFrutales = new THREE.Mesh(
    new THREE.PlaneBufferGeometry(1, 1, 100, 100),
    materialPortfolioFfFrutales,
);

const textureFfProductos = textureLoader.load('/portfolio/ffproductos.png');
const materialPortfolioFfProductos = new THREE.MeshBasicMaterial();
materialPortfolioFfProductos.map = textureFfProductos;
materialPortfolioFfProductos.side = THREE.DoubleSide;
const planeFfProductos = new THREE.Mesh(
    new THREE.PlaneBufferGeometry(1, 1, 100, 100),
    materialPortfolioFfProductos,
);

const textureFfBodas = textureLoader.load('/portfolio/ffbodas.png');
const materialPortfolioFfBodas = new THREE.MeshBasicMaterial();
materialPortfolioFfBodas.map = textureFfBodas;
materialPortfolioFfBodas.side = THREE.DoubleSide;
const planeFfBodas = new THREE.Mesh(
    new THREE.PlaneBufferGeometry(1, 1, 100, 100),
    materialPortfolioFfBodas,
);

const textureFfRomancedetalle = textureLoader.load('/portfolio/ffromancedetalle.png');
const materialPortfolioFfRomancedetalle = new THREE.MeshBasicMaterial();
materialPortfolioFfRomancedetalle.map = textureFfRomancedetalle;
materialPortfolioFfRomancedetalle.side = THREE.DoubleSide;
const planeFfRomancedetalle = new THREE.Mesh(
    new THREE.PlaneBufferGeometry(1, 1, 100, 100),
    materialPortfolioFfRomancedetalle,
);


const ambientLight = new THREE.AmbientLight(0xffffff, 0.5);

const lightPortfolio = new THREE.PointLight(0xffffff, 0.5);
lightPortfolio.position.x = 2;
lightPortfolio.position.y = 3;
lightPortfolio.position.z = 4;

planePgbUno.position.set(0, 0, 0);
planePgbQuienes.position.set(0, 0, 4);
planePgbContacto.position.set(-3, 0, 0);
planePgbSeguros.position.set(-3, 0, 4);
planeFfInicio.position.set(-1.5, 0, 0);
planeFfColecciones.position.set(-1.5, 0, 4);
planeFfFrutales.position.set(3, 0, 0);
planeFfProductos.position.set(3, 0, 4);
planeFfBodas.position.set(1.5, 0, 0);
planeFfRomancedetalle.position.set(1.5, 0, 4);

dodecahedronPortfolio.position.set(3, 0, 0);
scene.add(dodecahedronPortfolio);
scene.add(cubeFilosofia);

sphereContacto.position.set(-3, 0, 0);
scene.add(sphereContacto);

gui.add(cubeFilosofia.position, 'y');

const renderer = new THREE.WebGLRenderer({
    canvas: canvas,
    antialias: true
});

const onClick = (event) => {
    event.preventDefault();

    const cursor = new THREE.Vector2(
        (event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1,
    );

    raycaster.setFromCamera(cursor, camera);

    const [intersection] = raycaster.intersectObject(cubeFilosofia, true);

    const [intersectionSphere] = raycaster.intersectObject(sphereContacto, true);

    const [intersectiondodeca] = raycaster.intersectObject(
        dodecahedronPortfolio,
        true,
    );

    //const intersects = raycaster.intersectObjects(cubeFilosofia, sphereContacto, dodecahedronPortfolio);


    //const intersects = raycaster.intersectObjects(objectsToTest);

    /*
    if (intersects) {
        if (intersects.objectsToTest.name('cubeFilosofia')) {
            scene = sceneFilosofia;
        } else if (intersects.objectsToTest.name('sphereContacto')) {
            scene = sceneContacto;
        } else if (intersects.objectsToTest.name('dodecahedronPortfolio')) {
            scene = sceneDodecahedronPortfolio;
        }

    }
    */


    if (intersection) {

        scene.remove(cubeFilosofia);
        scene.remove(sphereContacto);
        scene.remove(dodecahedronPortfolio);

        scene = sceneFilosofia;

    } else {
        scene = scene;
    }

    if (intersectionSphere) {
        scene.remove(cubeFilosofia);
        scene.remove(sphereContacto);
        scene.remove(dodecahedronPortfolio);
        scene = sceneContacto;
    } else {
        scene = scene;
    }

    if (intersectiondodeca) {
        scene.remove(cubeFilosofia);
        scene.remove(sphereContacto);
        scene.remove(dodecahedronPortfolio);
        const textureLoader = new TextureLoader();



        scenePortfolio.add(planeFfRomancedetalle, planeFfBodas, ambientLight, lightPortfolio, planePgbUno, planePgbQuienes, planePgbContacto, planePgbSeguros, planeFfInicio, planeFfColecciones, planeFfFrutales, planeFfProductos);
        scene = scenePortfolio;
    } else {
        scene = scene;
    }

};

/**
 * Renderer
 */

renderer.setSize(sizes.width, sizes.height);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
renderer.domElement.addEventListener('click', onClick);

window.addEventListener('resize', () => {
    // Update sizes
    sizes.width = window.innerWidth;
    sizes.height = window.innerHeight;

    // Update camera
    camera.aspect = sizes.width / sizes.height;
    camera.updateProjectionMatrix();

    // Update renderer
    renderer.setSize(sizes.width, sizes.height);
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
});

const clock = new THREE.Clock()
let lastElapsedTime = 0

const tick = () => {
    // Call tick again on the next frame
    window.requestAnimationFrame(tick);

    const elapsedTime = clock.getElapsedTime();
    const deltaTime = elapsedTime - lastElapsedTime;
    lastElapsedTime = elapsedTime;

    //planePgbUno.rotation.set(
     //   Math.cos(deltaTime + Math.PI * 2) * 2,
     //   0,
     //   0,
    //);



    /*
    const rayOrigin = new THREE.Vector3(-3, 0, 0);
    const rayDirection = new THREE.Vector3(1, 0, 0);
    rayDirection.normalize();
    
    //raycaster.set(rayOrigin, rayDirection);

    const objectsToTest = [cubeFilosofia, sphereContacto, dodecahedronPortfolio];
    */

    // Update controls
    controls.update();

    renderer.clear();

    renderer.render(scene, camera);
    // Render

    /*if (scene == sceneContacto) {
        //camera.lookAt(scene.position);
        renderer.render(sceneContacto, camera);
    }
    */


    //camera.lookAt(scene.position);

    /*if (scene == sceneFilosofia) {
        textoFilosofia.sync(() => renderer.render(scene, camera));
        renderer.render(sceneFilosofia, camera);
    }
    */
}

tick()